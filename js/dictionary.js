/**
 * Created by slavkoglisic on 10/29/17.
 */
var dict = {
    "Home": {
        es: "Inicio"
    },
    "About Us": {
    es: "Nosotros"
    },
    "Services": {
        es: "Servicios"
    },
    "Technology": {
        es: "Tecnología"
    },
    "Career": {
        es: "Oportunidades laborales"
    },
    "Contact": {
    es: "Contacto"
    },
    "Our Services": {
        es: "Nuestros Servicios"
    },
    "We develop service technologies": {
        es:"Desarrollamos tecnologías de servicios"
    },
    "We design positive experiences": {
        es:"Diseñamos experiencias positivas"
    },
    "We facilitate the search for solutions" : {
        es:"Facilitamos la búsqueda de soluciones"
    },
    "We deliver efficient and reliable assistance" : {
        es:"Entregamos asistencia eficiente y confiable"
    },
    "We build value for your business by using the Smart Way": {
        es: "Desarrollamos valor para su negocio usando opciones inteligentes"
    },
    "Homes" : {
        es:"Hogar"
    },
    "Roadside" : {
        es:"Vial"
    },
    "Health" : {
        es:"Salud"
    },
    "Professional" : {
        es:"Profesional"
    },
    "CONTACT US": {
        es:"CONTÁCTENOS"
    },
    "Address": {
        es:"Dirección"
    },
    "Phones": {
        es:"Teléfonos"
    },
    "Language": {
        es:"Idioma"
    },
    "SUBMIT": {
        es:"ENVIAR"
    },
    "Partners": {
        es:"Partners"
    },
    "Products": {
        es:"Productos"
    },
    "Applications": {
        es: "Postulaciones"
    },
    "Mission": {
        es:"Misión"
    },
    "Vision": {
        es:"Visión"
    },
    "Objectives": {
        es:"Objetivos"
    },
    "Subject": {
        es:"Asunto"
    },
    "Name": {
        es:"Nombre"
    },
    "Business Name": {
        es:"Razón Social"
    },
    "Rut": {
        es:"Rut"
    },
    "Commercial address": {
        es:"Dirección Comercial"
    },
    "Phone": {
        es:"Teléfono"
    },
    "Email": {
        es:"Correo Electrónico"
    },
    "Specialty": {
        es:"Especialidad"
    },
    "Observations": {
        es:"Observaciones"
    },
    "Message": {
        es:"Mensaje"
    }
}







